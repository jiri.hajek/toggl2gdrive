package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
)

var (
	togglAPIToken        string
	togglTargetWorkspace string
	togglTargetClient    string
	abbrev               string
	driveTargetFolder    string
	allConfigPaths       []string
	configPath           string
)

func setupEnvironment() {
	log.SetOutput(os.Stdout)
	log.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))

	xdgConfigHome := os.Getenv("XDG_CONFIG_HOME")
	home := os.Getenv("HOME")

	allConfigPaths = []string{
		".",
		fmt.Sprintf("%s/toggl2gdrive", xdgConfigHome),
		fmt.Sprintf("%s/.toggl2gdrive", home),
	}

	var err error
	for _, path := range allConfigPaths {
		configFilePath := fmt.Sprintf("%s/config", path)
		err = godotenv.Load(configFilePath)
		if err == nil {
			configPath = path
			log.Printf("Using configuration from %s", configPath)
			break
		}
	}

	if err != nil {
		log.Fatalln("Could not find a configuration file.")
	}

	var ok bool
	togglAPIToken, ok = os.LookupEnv("TOGGL_API_TOKEN")
	if !ok {
		log.Fatalln("Missing TOGGL_API_TOKEN environment variable.")
	}

	togglTargetWorkspace, ok = os.LookupEnv("TOGGL_TARGET_WORKSPACE")
	if !ok {
		log.Fatalln("Missing TOGGL_TARGET_WORKSPACE environment variable.")
	}

	togglTargetClient, ok = os.LookupEnv("TOGGL_TARGET_CLIENT")
	if !ok {
		log.Fatalln("Missing TOGGL_TARGET_CLIENT environment variable.")
	}

	abbrev, ok = os.LookupEnv("ABBREV")
	if !ok {
		log.Fatalln("Missing ABBREV environment variable.")
	}

	driveTargetFolder, ok = os.LookupEnv("DRIVE_TARGET_FOLDER")
	if !ok {
		log.Fatalln("Missing DRIVE_TARGET_FOLDER environment variable.")
	}
}

func getEndOfLastMonth() time.Time {
	now := time.Now()
	currentYear, currentMonth, _ := now.Date()

	endOfLastMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, now.Location()).
		Add(-time.Nanosecond)

	return endOfLastMonth
}

func main() {
	setupEnvironment()

	tgl := newToggl(togglAPIToken, togglTargetWorkspace, togglTargetClient)
	tgl.setWorkspace()
	tgl.setClient()

	endOfLastMonth := getEndOfLastMonth()
	dayFrom := 1
	targetYear, targetMonth, dayTo := endOfLastMonth.Date()
	from := fmt.Sprintf("%d-%02d-%02d", targetYear, targetMonth, dayFrom)
	to := fmt.Sprintf("%d-%02d-%02d", targetYear, targetMonth, dayTo)

	summaryReportReadCloser := tgl.getSummaryReport(from, to)
	defer summaryReportReadCloser.Close()

	detailsReportReadCloser := tgl.getDetailedReport(from, to)
	defer detailsReportReadCloser.Close()

	targetYearAbbrev := targetYear % 100
	namePrefix := fmt.Sprintf("%02d_%02d_%s_Toggl_", targetYearAbbrev, targetMonth, abbrev)
	summaryReportName := fmt.Sprintf("%sSummary.pdf", namePrefix)
	detailsReportName := fmt.Sprintf("%sDetail.pdf", namePrefix)

	driveService, err := getService()
	if err != nil {
		log.Fatalln(err)
	}

	driveTargetFolderID, err := getDriveFolderID(driveService, driveTargetFolder, abbrev)
	if err != nil {
		log.Fatalf("Could not get drive target folder ID: %s\n", err)
	}

	log.Println("Uploading summary report ...")
	_, err = createFileInDirectory(driveService, summaryReportName, "application/pdf", summaryReportReadCloser, driveTargetFolderID)
	if err != nil {
		log.Fatalf("Could not upload summary report: %s\n", err)
	}

	log.Println("Uploading detailed report ...")
	_, err = createFileInDirectory(driveService, detailsReportName, "application/pdf", detailsReportReadCloser, driveTargetFolderID)
	if err != nil {
		log.Fatalf("Could not upload details report: %s\n", err)
	}

	log.Println("Reports uploaded successfully!")
}
