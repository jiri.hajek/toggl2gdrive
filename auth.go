package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"time"

	"golang.org/x/oauth2"
)

// Opens the specified URL in the default browser of the user.
func openURL(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default:
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

// Retrieves and saves token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := fmt.Sprintf("%s/token.json", configPath)
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}

	return config.Client(context.Background(), tok)
}

// Requests token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)

	server := http.Server{
		Addr:    ":3000",
		Handler: nil,
	}

	tokenChannel := make(chan string)
	defer close(tokenChannel)

	redirectHandler := func(w http.ResponseWriter, r *http.Request) {
		authCode := r.URL.Query().Get("code")

		if authCode != "" {
			tokenChannel <- authCode
			io.WriteString(
				w, "<html><body><h1>You can now close this tab.</h1></body></html>",
			)
		} else {
			io.WriteString(
				w, "<html><body><h1>Error: missing authorization code</h1></body></html>",
			)
		}
	}

	http.HandleFunc("/", redirectHandler)

	serverErrorChannel := make(chan error)
	go func() {
		err := server.ListenAndServe()
		if err != nil {
			serverErrorChannel <- err
		}
	}()

	defer server.Close()

	go func() {
		time.Sleep(100 * time.Millisecond)
		log.Printf("Visit the following URL to authorize toggl2gdrive:\n%s\n", authURL)
		openURL(authURL)
	}()

	var authCode string

	select {
	case authCode = <-tokenChannel:
		server.Close()
		<-serverErrorChannel
		close(serverErrorChannel)
	case err := <-serverErrorChannel:
		log.Fatalf("HTTP server error: %s", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web %v", err)
	}

	log.Println("Authorization successful")

	return tok
}

// Retrieves token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves token to a file path.
func saveToken(path string, token *oauth2.Token) {
	log.Printf("Saving token to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
