package main

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"

	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/option"
)

func getService() (*drive.Service, error) {
	credentialsFilePath := fmt.Sprintf("%s/credentials.json", configPath)
	credentials, err := ioutil.ReadFile(credentialsFilePath)
	if err != nil {
		return nil, err
	}

	config, err := google.ConfigFromJSON(credentials, drive.DriveMetadataReadonlyScope, drive.DriveFileScope)

	if err != nil {
		return nil, err
	}

	client := getClient(config)

	service, err := drive.NewService(context.Background(), option.WithHTTPClient(client))

	if err != nil {
		return nil, err
	}

	return service, err
}

func createFileInDirectory(service *drive.Service, name string, mimeType string, content io.Reader, parentID string) (*drive.File, error) {
	f := &drive.File{
		MimeType: mimeType,
		Name:     name,
		Parents:  []string{parentID},
	}
	file, err := service.Files.Create(f).Media(content).Do()

	if err != nil {
		return nil, err
	}

	return file, nil
}

func createFile(service *drive.Service, name string, mimeType string, content io.Reader) (*drive.File, error) {
	f := &drive.File{
		MimeType: mimeType,
		Name:     name,
	}
	file, err := service.Files.Create(f).Media(content).Do()

	if err != nil {
		return nil, err
	}

	return file, nil
}

func getDriveFolderID(service *drive.Service, folderName string, abbrev string) (string, error) {
	baseQueryCall := service.Files.List().SupportsAllDrives(true)
	isFolderQuery := "mimeType = 'application/vnd.google-apps.folder'"

	targetQuery := fmt.Sprintf("name = '%s' and %s", folderName, isFolderQuery)
	targetQueryResult, err := baseQueryCall.Q(targetQuery).Do()
	if err != nil {
		return "", err
	}

	var folderID string
	if len(targetQueryResult.Files) > 0 {
		folderID = targetQueryResult.Files[0].Id
	} else {
		defaultQuery := fmt.Sprintf("name = 'Faktury_%s' and %s", abbrev, isFolderQuery)
		defaultQueryResult, err := baseQueryCall.Q(defaultQuery).Do()
		if err != nil {
			return "", err
		}

		if len(defaultQueryResult.Files) > 0 {
			return "", fmt.Errorf("folder %q does not exist, did you mean %q?", folderName, defaultQueryResult.Files[0].Name)
		}

		return "", fmt.Errorf("folder %q does not exist", folderName)
	}

	return folderID, nil
}
