# toggl2gdrive
## This project has been retired in favor of [*eztoggl*](https://gitlab.com/jiri.hajek/eztoggl), which can do everything *toggl2gdrive* does and more.

*toggl2gdrive* is a command-line tool that generates both summary and detailed Toggl reports for the last month and uploads them to a specified Google Drive folder.

**NOTE:** The Google app whose credentials are specified in `credentials.json` is limited to internal use and therefore will only work for Google accounts in the `*@applifting.cz` domain. If you'd like to use this on other accounts, you'll have to create your own Google Cloud Platform project, then create *OAuth client ID* credentials [here](https://console.cloud.google.com/apis/credentials), download them and replace the existing `credentials.json`. When prompted for *Application type*, select *Web application* and add `http://localhost:3000` to the list of *Authorized redirect URIs*.

## Google Drive permissions
*toggl2gdrive* requires 2 scopes for the Google Drive API v3, specifically `drive.metadata.readonly` and `drive.file`. All of the scopes along with short descriptions can be found [here](https://developers.google.com/identity/protocols/oauth2/scopes#drive).

`drive.metadata.readonly` grants the app read-only access to metadata of files in your Google Drive. It is needed to find the target folder where your Toggl reports should be uploaded.

`drive.file` allows the app to view and manage Google Drive files and folders that have been opened or created by the app. It is needed to upload your Toggl reports.

## Installation
The easiest way to install *toggl2gdrive* is by using `go install`.
```
go install gitlab.com/jiri.hajek/toggl2gdrive
```

You could also clone the repository, go through the source code and then build the app.
```
git clone https://gitlab.com/jiri.hajek/toggl2gdrive.git
cd toggl2gdrive
make build
```

If you don't want to build the app yourself, you can grab the latest binary from [releases](https://gitlab.com/jiri.hajek/toggl2gdrive/-/releases).

## Configuration
Create a `config` file based on the configuration example in `config.example`.

*toggl2gdrive* attempts to load configuration from the following directories, in this specific order:
1. `.` (current directory)
2. `$XDG_CONFIG_HOME/toggl2gdrive`
3. `$HOME/.toggl2gdrive`

The first encountered directory that contains a `config` file will also be used for `credentials.json` and `token.json`.

| Variable | Description |
|:---------|:------------|
| ABBREV   | A text that will be included in the filenames of your Toggl reports, e.g. `21_03_ABBREV_Toggl_Summary.pdf` |
| DRIVE\_TARGET\_FOLDER | Name of the Google Drive folder where you want your Toggl reports to be uploaded, the folder must be located at the root of your Google Drive (i.e. no folders inside of folders) |
| TOGGL\_API\_TOKEN | Your Toggl API token |
| TOGGL\_TARGET\_CLIENT | Name of the Toggl client you want to generate the reports for |
| TOGGL\_TARGET\_WORKSPACE | Name of the Toggl workspace you want to generate the reports for |

## Usage
![demo](images/demo.gif)

When you run *toggl2gdrive* for the first time, it will open a browser tab where you'll have to authenticate with Google and give the app permissions mentioned above. Once that is done, your access and refresh tokens will be saved to `token.json` and your Toggl reports uploaded to the specified Google Drive folder.

As long as `token.json` exists, the app will not prompt you for authorization and will instead refresh the existing token as needed. Keep in mind that the app only receives your refresh token on the first authorization so if you lose your `token.json`, you'll have to remove the app's access from your Google account's security settings first, and then go through the authorization flow again, otherwise you will be stuck with an access token that only lasts for 1 hour.

For even more convenience, you can set *toggl2gdrive* to automatically run each month with something like *cron* or *anacron*.
```
# crontab
# Upload Toggl reports to Google Drive on the 1st day of each month at 6 AM
0 6 1 * * /path/to/toggl2gdrive
```
