module gitlab.com/jiri.hajek/toggl2gdrive

go 1.15

require (
	cloud.google.com/go v0.77.0 // indirect
	github.com/joho/godotenv v1.3.0
	golang.org/x/oauth2 v0.0.0-20210216194517-16ff1888fd2e
	google.golang.org/api v0.40.0
)
