package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
)

const (
	userAgent string = "toggl2gdrive"
)

type togglWorkspace struct {
	ID   int
	Name string
}

type togglClient struct {
	ID   int
	Name string
}

type toggl struct {
	apiToken      string
	workspaceName string
	workspaceID   string
	clientName    string
	clientID      string
}

func newToggl(apiToken string, workspaceName string, clientName string) *toggl {
	tgl := new(toggl)
	tgl.apiToken = apiToken
	tgl.workspaceName = workspaceName
	tgl.clientName = clientName
	return tgl
}

func (tgl *toggl) getSummaryReport(from string, to string) io.ReadCloser {
	url := fmt.Sprintf(
		"https://api.track.toggl.com/reports/api/v3/workspace/%s/summary/time_entries.pdf",
		tgl.workspaceID,
	)

	clientIDInt, err := strconv.ParseInt(tgl.clientID, 10, 0)
	if err != nil {
		log.Fatalf("Could not convert client ID %q to int", tgl.clientID)
	}

	payload, err := json.Marshal(map[string]interface{}{
		"start_date":      from,
		"end_date":        to,
		"client_ids":      []int64{clientIDInt},
		"order_by":        "title",
		"order_dir":       "asc",
		"grouping":        "projects",
		"sub_grouping":    "time_entries",
		"duration_format": "decimal",
		"date_format":     "DD/MM/YYYY",
		"hide_amounts":    true,
		"hide_rates":      true,
	})
	if err != nil {
		log.Fatalln(err)
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	if err != nil {
		log.Fatalln(err)
	}

	setTogglRequestHeaders(req, tgl)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("Could not get summary report: %s\n", err)
	}
	if res.StatusCode != 200 {
		log.Fatalf("Could not get summary report: server responded with code %d\n", res.StatusCode)
	}

	return res.Body
}

func (tgl *toggl) getDetailedReport(from string, to string) io.ReadCloser {
	url := fmt.Sprintf(
		"https://api.track.toggl.com/reports/api/v2/details.pdf?workspace_id=%s&user_agent=%s&client_ids=%s&display_hours=decimal&order_desc=off&since=%s&until=%s",
		tgl.workspaceID,
		userAgent,
		tgl.clientID,
		from,
		to,
	)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalln(err)
	}

	setTogglRequestHeaders(req, tgl)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("Could not get detailed report: %s\n", err)
	}
	if res.StatusCode != 200 {
		log.Fatalf("Could not get detailed report: server responded with code %d\n", res.StatusCode)
	}

	return res.Body
}

func (tgl *toggl) setWorkspace() {
	url := "https://api.track.toggl.com/api/v8/workspaces"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalln(err)
	}

	setTogglRequestHeaders(req, tgl)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("Could not get workspaces: %s\n", err)
	}
	if res.StatusCode != 200 {
		log.Fatalf("Could not get workspaces: server responded with code %d\n", res.StatusCode)
	}

	defer res.Body.Close()

	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Could not read workspaces: %s\n", err)
	}

	var workspaces []togglWorkspace
	json.Unmarshal(bytes, &workspaces)

	found := false
	for _, workspace := range workspaces {
		if workspace.Name == tgl.workspaceName {
			tgl.workspaceID = fmt.Sprint(workspace.ID)
			found = true
			break
		}
	}

	if !found {
		if len(workspaces) == 1 {
			log.Fatalf("Workspace %q does not exist. Did you mean %q?\n", tgl.workspaceName, workspaces[0].Name)
		}

		log.Fatalf("Workspace %q does not exist\n", tgl.workspaceName)
	}
}

func (tgl *toggl) setClient() {
	url := fmt.Sprintf("https://api.track.toggl.com/api/v8/workspaces/%s/clients", tgl.workspaceID)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalln(err)
	}

	setTogglRequestHeaders(req, tgl)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("Could not get clients: %s\n", err)
	}
	if res.StatusCode != 200 {
		log.Fatalf(
			"Could not get clients: server responded with code %d\n", res.StatusCode,
		)
	}

	defer res.Body.Close()

	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Could not read clients: %s\n", err)
	}

	var clients []togglClient
	json.Unmarshal(bytes, &clients)

	found := false
	for _, client := range clients {
		if client.Name == tgl.clientName {
			tgl.clientID = fmt.Sprint(client.ID)
			found = true
			break
		}
	}

	if !found {
		if len(clients) == 1 {
			log.Fatalf(
				"Client %q does not exist. Did you mean %q?\n", tgl.clientName, clients[0].Name,
			)
		}

		log.Fatalf("Client %q does not exist\n", tgl.clientName)
	}
}

func setTogglRequestHeaders(req *http.Request, tgl *toggl) {
	req.Header.Add("Content-Type", "application/json")
	req.SetBasicAuth(tgl.apiToken, "api_token")
}
